/**
 * The class representing combined values
 *
 * If will first check to see if the value contains only of numbers and
 * operators, in that case the expression will be evaluated and return a Simple
 * Value otherwise the expression will be evaluated later.
 *
 * @author shahin
 */
public class CombinedValue extends CellValue {

	private Double mValue;
	private String mExpressionValue;

	private CombinedValue(String value) {
		this.setValue(value);
	}

	@Override
	public Double getValue() {
		return mValue;
	}

	@Override
	public void setValue(String value) {
		this.mExpressionValue = value;
	}

	@Override
	public void setValue(Double value) {
		this.mValue = value;
	}

	public static CellValue get(String value) {
		if (value.replaceAll("[\\s*+/-]", "").matches("\\d+")) {
			// When only numbers and operators
			return new NumericValue(Expression.evaluate(value));
		}
		return new CombinedValue(value);
	}

	@Override
	public int evaluate(SpecSheet sheet) {
		String[] components = mExpressionValue.split(" ");
		StringBuilder referencesResolved = new StringBuilder();
		for (String component : components) {
			if (component.matches("\\d+")) {
				// If a number, it is already resolved :)
				referencesResolved.append(component);
			} else if (component.matches("[/*+-]")) {
				// if operator, just add it
				referencesResolved.append(component);
			} else {
				// reference to a cell in the sheet
				int row = (component.codePointAt(0) % 91) - 65; // A = 65, Z =
				int col = Integer.valueOf(component.substring(1)) - 1;
				try {
					// get the value at that cell and append
					referencesResolved.append(sheet.valueAt(row, col));
				} catch (ExceptionHandler ce) {
					this.setValue("Cyclic expression: " + this.mExpressionValue);
					throw ce;
				}
			}
			referencesResolved.append(" ");
		}
		// All the cell references have been replaced with numeric values now
		// the value may be a number or an expression

		if (!referencesResolved.toString().trim().equals("")) {
			if (referencesResolved.toString().replaceAll("[\\s.]", "").matches("\\d+")) {
				// all numeric, return simple value
				this.mValue = Double.valueOf(referencesResolved.toString());
			} else {
				// we now have a postfix expression to evaluate
				this.mValue = Double.valueOf(Expression.evaluate(referencesResolved.toString().trim()));
			}
		}
		return 0;
	}
}
