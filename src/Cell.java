/**
 * A class called Cell is designed to store the cell value
 *
 * @author shahin
 */
public class Cell {
	private CellValue mCellValue;
	private STATE mValueState;

	public Cell(CellValue value) {
		this.mCellValue = value;
		this.mValueState = STATE.NONE;
	}

	public int evaluate(SpecSheet sheet) throws ExceptionHandler {
		if (this.mValueState == STATE.INPROGRESS) {
			this.mCellValue.setValue("Cycle detected");
			this.mValueState = STATE.ERROR;
			throw new ExceptionHandler();
		}
		if (this.mValueState == STATE.ERROR) {
			return 1;
		}
		this.mValueState = STATE.INPROGRESS;
		int returnCode = this.mCellValue.evaluate(sheet);
		if (returnCode == 0)
			this.mValueState = STATE.EVALUATED;
		return returnCode;
	}

	/**
	 * Returns the value in this cell. If it has not been evaluated, it will be
	 * evaluated now
	 *
	 * @param sheet
	 * @return String representation of the value of the cell
	 * @throws ExceptionHandler
	 */
	public String getValue(SpecSheet sheet) throws ExceptionHandler {
		if (this.mValueState != STATE.EVALUATED) {
			// If the value for this cell has not yet been evaluated do it now
			evaluate(sheet);
		}
		if (this.mValueState == STATE.ERROR || this.mValueState == STATE.INPROGRESS) {
			throw new ExceptionHandler();
		}
		if (this.mCellValue.getValue() == null)
			return "";
		return this.mCellValue.getValue().toString();
	}

	public STATE getValueState() {
		return mValueState;
	}

	/**
	 * Represents the current state of the cell
	 */
	public enum STATE {
		EVALUATED, INPROGRESS, NONE, ERROR
	}

	public String toString() {
		return this.mCellValue.toString();
	}
}
