import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * A class to represent the sheet.
 *
 * @author shahin
 */
public class Spreadsheet {

	private static final String FORMAT = "%.5f";

	/**
	 * Evaluate the input that was received
	 *
	 * @param lines
	 */
	public static SpecSheet analyse(String[] lines) {
		String[] sheetSpec = lines[0].split(" ");
		SpecSheet sheet = new SpecSheet(Integer.parseInt(sheetSpec[1]), Integer.parseInt(sheetSpec[0]));

		for (int lineNo = 1; lineNo < lines.length; lineNo++) {
			int row = (lineNo - 1) / sheet.getCols();
			int col = (lineNo - 1) % sheet.getCols();
			sheet.put(row, col, lines[lineNo]);
		}
		// analyse cells
		sheet.analyse();
		return sheet;
	}

	private static void print(SpecSheet sheet) {
		System.out.println(sheet.getCols() + " " + sheet.getRows());
		for (int row = 0; row < sheet.getRows(); row++) {
			for (int col = 0; col < sheet.getCols(); col++) {
				System.out.println(String.format(FORMAT, Double.valueOf(sheet.valueAt(row, col))));
			}
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		List<String> dataList = new ArrayList<String>();
		while (scanner.hasNext()) {
			dataList.add(scanner.nextLine());
		}
		scanner.close();
		String[] dataArray = new String[dataList.size()];
		SpecSheet sheet = analyse(dataList.toArray(dataArray));
		print(sheet);
	}
}

