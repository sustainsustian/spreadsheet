/**
 * This class represents a simple Number is stored in the cell. No further
 * calculation required
 *
 * @author shahin
 *
 */
public class NumericValue extends CellValue {

	private Double mNumericValue;

	public NumericValue(String value) {
		this.setValue(value);
	}

	@Override
	public void setValue(String value) {
		if (value == null || value.trim().equals(""))
			return;
		this.setValue(Double.parseDouble(value));
	}

	@Override
	public void setValue(Double value) {
		this.mNumericValue = value;
	}

	@Override
	public Double getValue() {
		return this.mNumericValue;
	}

	@Override
	public int evaluate(SpecSheet parent) {
		return 0;
	}
}
