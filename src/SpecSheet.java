/**
 * A class to represent the sheet.
 *
 * @author shahin
 */
public class SpecSheet {
	private Cell[][] mCells;
	private int mRows = 0;
	private int mColumns = 0;

	public SpecSheet(int rows, int coloumns) {
		this.mRows = rows;
		this.mColumns = coloumns;
		this.mCells = new Cell[rows][coloumns];
	}

	public int getRows() {
		return mRows;
	}

	public void setRows(int rows) {
		this.mRows = rows;
	}

	public int getCols() {
		return mColumns;
	}

	public void setCols(int cols) {
		this.mColumns = cols;
	}

	/**
	 * Add the <code>cellValue</code> at the specified <code>row</code> and
	 * <code>col</code>
	 *
	 * @param row
	 * @param col
	 * @param cellValue
	 * @return
	 */
	public SpecSheet put(int row, int col, String cellValue) {
		this.mCells[row][col] = new Cell(CellValue.get(cellValue));
		return this;
	}

	/**
	 * Goes through every cell in the sheet and evaluates all the cells once
	 * again. Any composite references still not evaluated will be evaluated now
	 */
	public void analyse() {
		for (int row = 0; row < this.mRows; row++) {
			for (int col = 0; col < this.mColumns; col++) {
				int result = this.mCells[row][col].evaluate(this);
				if (result != 0)
					return; // In case of a non zero error code, stop processing
			}
		}
	}

	/**
	 * Return the value at <code>row</code> and <code>col</code> If the value
	 * has not yet been evaluated, tries to process it right now
	 *
	 * @param row
	 * @param col
	 * @return the value at the cell in String format
	 */
	public String valueAt(int row, int col) {
		Cell cell = this.mCells[row][col];
		return cell.getValue(this);
	}

	public String toString() {
		StringBuilder sheet = new StringBuilder("<Sheet>\n");
		for (int row = 0; row < this.mRows; row++) {
			for (int col = 0; col < this.mColumns; col++) {
				sheet.append("\t");
				sheet.append(mCells[row][col].toString());
				sheet.append("\t");
			}
			sheet.append("\n");
		}
		sheet.append("</Sheet>\n");
		return sheet.toString();
	}
}
